package application.service;

import application.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ReviewService extends JpaRepository<Review,Long> {

}
