package application.service;

import application.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientService  extends JpaRepository<Client,Long> {
}
