package application.service;

import application.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface ReservationService extends JpaRepository<Reservation,Long> {
}
