package application.controller;

import application.entity.Reservation;
import application.entity.Review;
import application.service.ReservationService;
import application.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/reservation")
public class ReservationController {

        @Autowired
        private ReservationService reservationService;

        @PostMapping
        @ResponseBody
        ResponseEntity<Reservation> create(@RequestBody Reservation reservation) throws Exception {
            return new ResponseEntity<>(reservationService.save(reservation), HttpStatus.OK);
        }


        @GetMapping()
        List<Reservation> read(){
            return reservationService.findAll();
        }
}
