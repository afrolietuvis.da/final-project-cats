package application.controller;

import application.entity.Client;
import application.entity.Review;
import application.service.ClientService;
import application.service.ReviewService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/review")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @PostMapping
    @ResponseBody
    ResponseEntity<Review> create(@RequestBody Review review) throws Exception {
        return new ResponseEntity<>(reviewService.save(review), HttpStatus.OK);
    }


    @GetMapping()
    List<Review> read(){
        return reviewService.findAll();
    }
}
