package application.controller;

import application.DTO.ClientDTO;
import application.entity.Client;
import application.service.ClientService;
import application.utils.ClientValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/client")
public class ClientController {
@Autowired
private ClientService clientService;
@Autowired ClientValidator clientValidator;

    @RequestMapping(method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<Client> create(@RequestBody ClientDTO clientDTO) throws Exception {
        clientValidator.validate(clientDTO);
        return new ResponseEntity<>(clientService.save(Client.of(clientDTO)), HttpStatus.OK);
    }

    @GetMapping
    @CrossOrigin(origins = "http://localhost:4200")
    List<Client> read(){
        return clientService.findAll();
    }

    @GetMapping("/{id}")
    public Client get(@PathVariable("id") long id) {
        return clientService.getOne(id);
    }


}
