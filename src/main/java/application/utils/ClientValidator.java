package application.utils;

import application.DTO.ClientDTO;
import application.controller.ClientController;
import application.entity.Client;
import application.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientValidator {

   @Autowired
   ClientService clientService;

    public void validate(ClientDTO clientDTO){
        List<Client> clients = clientService.findAll();
        List<String> emails = clients.stream().map(Client::getEmail).collect(Collectors.toList());
        List<String> numbers = clients.stream().map(Client::getPhone).collect(Collectors.toList());
        String inputPhone = clientDTO.getPhone();
        String inputEmail = clientDTO.getEmail();

        if(inputEmail.contains("@") && inputEmail.endsWith(".com"))
        {

            if (emails.contains(inputEmail)) throw new InvalidParameterException("User with this email already exists!");
            if(numbers.contains(inputPhone)) throw new InvalidParameterException("User with this phone already exists!");
        }
        else {
            throw new InvalidParameterException("Invalid email address");
        }


    }
}
