package application.utils;

import application.entity.Client;

public interface Validator {

  void validate(Client client);
  void validate(String string);
}
