package application.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler","client"})
@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @Column(name ="id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="date_from")
    private LocalDate startDate;

    @Column(name="date_to")
    private LocalDate endDate;

    @Column(name="is_paid")
    private boolean isPaid;


    @ManyToOne
    @JoinColumn(name ="room_id",nullable = false)
    @JsonBackReference
    private Room room;

    @ManyToOne
    @JoinColumn(name="client_id",nullable = false)
    @JsonBackReference
    private Client client;

    public Reservation(){

    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return
                "Reservation ID: " + id +
                " Checkin date:" + startDate +
                " Checkout date: " + endDate +
                " Room: " + room;
    }
}
