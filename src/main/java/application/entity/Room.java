package application.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler","hotel","reservations"})
@Entity
@Table(name = "room")
public class Room {

    @Id
    @Column(name = "id",insertable = false,updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "room_type")
    private String roomType;

    @Column(name = "number")
    private int roomNumber;

    @Column(name = "floor",insertable = false,updatable = false)
    private int floorNumber;

    @ManyToOne
    @JoinColumn(name = "hotel_id",nullable = false,insertable = false,updatable = false)
    @JsonBackReference
    private Hotel hotel;

    @OneToMany
    @JoinColumn(name ="room_id")
    @JsonBackReference
    private List<Reservation> reservations = new ArrayList<>();

    @Column(name ="price",nullable = false)
    private int price;


    @Column(name = "description",nullable = false)
    private String description;

    @Column(name ="maximum_guests",nullable = false)
    private int maximumGuests;

    @OneToMany
    @JoinColumn(name = "room_id")
    @JsonManagedReference
    private List<Review> reviews = new ArrayList<>();

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public int getMaximumGuests() {
        return maximumGuests;
    }

    public void setMaximumGuests(int maximumGuests) {
        this.maximumGuests = maximumGuests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Room() {
    }

    @Override
    public String toString() {
        return "Room type: " + getRoomType() + ", Floor number: " + getFloorNumber()
                + ", Room number: " + getRoomNumber() + "\nDescription: " +description;
    }

}
