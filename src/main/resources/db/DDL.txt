-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema catshotel
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema catshotel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `catshotel` DEFAULT CHARACTER SET utf8 ;
USE `catshotel` ;

-- -----------------------------------------------------
-- Table `catshotel`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catshotel`.`client` ;

CREATE TABLE IF NOT EXISTS `catshotel`.`client` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `registration_date` DATE NULL,
  `date_modified` DATE NULL,
  `loyalty_points` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catshotel`.`hotel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catshotel`.`hotel` ;

CREATE TABLE IF NOT EXISTS `catshotel`.`hotel` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catshotel`.`loyalty_card`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catshotel`.`loyalty_card` ;

CREATE TABLE IF NOT EXISTS `catshotel`.`loyalty_card` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `loyalty_level` VARCHAR(45) NOT NULL,
  `client_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_loyalty_card_client1_idx` (`client_id` ASC) VISIBLE,
  CONSTRAINT `fk_loyalty_card_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `catshotel`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catshotel`.`room`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catshotel`.`room` ;

CREATE TABLE IF NOT EXISTS `catshotel`.`room` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `floor` INT NOT NULL,
  `number` INT NOT NULL,
  `room_type` VARCHAR(45) NOT NULL,
  `hotel_id` BIGINT NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  `maximum_guests` INT NOT NULL,
  `price` DECIMAL(10,0) NOT NULL,
  `rating` DOUBLE NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `fk_room_hotel1_idx` (`hotel_id` ASC) VISIBLE,
  CONSTRAINT `fk_room_hotel1`
    FOREIGN KEY (`hotel_id`)
    REFERENCES `catshotel`.`hotel` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catshotel`.`reservation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catshotel`.`reservation` ;

CREATE TABLE IF NOT EXISTS `catshotel`.`reservation` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `date_from` DATE NOT NULL,
  `date_to` DATE NOT NULL,
  `is_paid` BIT(1) NOT NULL,
  `room_id` BIGINT NOT NULL,
  `client_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reservation_room1_idx` (`room_id` ASC) VISIBLE,
  INDEX `fk_reservation_client1_idx` (`client_id` ASC) VISIBLE,
  CONSTRAINT `fk_reservation_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `catshotel`.`client` (`id`),
  CONSTRAINT `fk_reservation_room1`
    FOREIGN KEY (`room_id`)
    REFERENCES `catshotel`.`room` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `catshotel`.`review`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `catshotel`.`review` ;

CREATE TABLE IF NOT EXISTS `catshotel`.`review` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `comment` VARCHAR(300) NOT NULL,
  `room_id` BIGINT NOT NULL,
  `client_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_review_room1_idx` (`room_id` ASC) VISIBLE,
  INDEX `fk_review_client1_idx` (`client_id` ASC) VISIBLE,
  CONSTRAINT `fk_review_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `catshotel`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_review_room1`
    FOREIGN KEY (`room_id`)
    REFERENCES `catshotel`.`room` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
